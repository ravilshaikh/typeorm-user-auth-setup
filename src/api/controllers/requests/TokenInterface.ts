export interface TokenInterface {
    userId: number;
    email: string;
    firstName: string;
    lastName: string;
    mobileNumber?: string;
}
