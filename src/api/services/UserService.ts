import * as jwt from 'jsonwebtoken';
import { sign } from 'jsonwebtoken';
import { BadRequestError, HttpError } from 'routing-controllers';
import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { env } from '../../../src/env';
import { AuthResponse } from '../../auth/AuthService';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { User } from '../models/User';
import { UserRepository } from '../repositories/UserRepository';
import { MailService } from './MailService';
@Service()
export class UserService {
    constructor(
        @OrmRepository() private userRepository: UserRepository,
        @Logger(__filename) private log: LoggerInterface,
        @Service() private mailService :MailService
    ) {}

    public async findByEmail(email: string): Promise<User> {
        this.log.info('Find one user');
        const user = await this.userRepository.findOne({
            where: {
                isDeleted: false,
                email: email,
            },
        });
        return user;
    }

    public async findById(userId: number): Promise<User> {
        this.log.info('Find one user');
        const user = await this.userRepository.findOne(userId, {
            where: {
                isDeleted: false,
            },
        });
        return user;
    }

    public async create(user: User): Promise<User> {
        this.log.info('Create a new user => ', user.toString());
        try {
            const newUser = await this.userRepository.save(user);
            if (newUser) {
                const token = jwt.sign({ userId: user.id }, env.jwt.SECRET, {
                    expiresIn: env.jwt.EXPIRES_IN,
                });
                this.mailService.createAccount(
                    user.firstName,
                    user.lastName,
                    user.email,
                    token,
                );
            }
            return newUser;
        } catch (err) {
            if (err.code && err.code === 'ER_DUP_ENTRY') {
                throw new BadRequestError(
                    'This email is already registered with us',
                );
            }
            throw new HttpError(500, 'Internal Server Error');
        }
    }

    public findUserProfile(req: any): Promise<User> {
        this.log.info('Fetching loggedIn user profile' + req.user.toString());
        const id = req.user.userId;
        return this.userRepository.findOne(id);
    }

    public async verifyUserEmail(req: any): Promise<AuthResponse> {
        const id = req.user.userId;
        let user = new User();
        user.id = id;
        await this.userRepository.update(id, { isEmailValidated: true });
        const token = sign(
            {
                userId: user.id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                mobileNumber:user.mobileNumber
            },
            env.jwt.SECRET,
            { expiresIn: env.jwt.EXPIRES_IN },
        );
        const authResponse = new AuthResponse();
        authResponse.jwt = token;
        return authResponse;
    }

    public async resetPassword(req: any, password: string): Promise<void> {
        const id = req.user.userId;
        const user = new User();
        user.id = id;
        user.password = await User.hashPassword(password);
        console.log("reset-passowrd",user.id,user);
       const data=await this.userRepository.query(`UPDATE users set password = ? where id =?`,[user.password,user.id]);
       console.log(data);
       // await this.userRepository.save(user);
        return;
    }
}
