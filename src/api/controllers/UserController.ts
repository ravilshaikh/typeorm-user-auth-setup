import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';
import { Request } from 'express';
import {
    Authorized,
    Body,
    Get,
    JsonController,
    OnUndefined,
    Post,
    Req,
} from 'routing-controllers';
import { Service } from 'typedi/decorators/Service';
import { UserNotFoundError } from '../errors/UserNotFoundError';
import { User } from '../models/User';
import { UserService } from '../services/UserService';
export class userRequired {
    @MinLength(5)
    public password: string;
    @IsNotEmpty()
    public firstName: string;
    public lastName: string;
    public mobileNumber: string;
    @IsEmail()
    public email: string;
}

@JsonController('/user')
export class UserController {
    constructor(
        @Service() private userService: UserService,
    ) {}

    @Authorized()
    @Get('/profile')
    @OnUndefined(UserNotFoundError)
    public getProfile(@Req() req: Request): Promise<User> {
        console.log(req['user']);
        return this.userService.findUserProfile(req);
    }


    @Post('/create')
    @OnUndefined(UserNotFoundError)
    public async create(@Body() body: userRequired): Promise<User> {
        let user = new User();
        body['isEmailValidated'] = false;
        user = Object.assign(user, body);
        const data = await this.userService.create(user);
        data['messageKey'] =
            'Your account is created successfully. Please check your inbox to verify your email';
        return data;
    }

}
