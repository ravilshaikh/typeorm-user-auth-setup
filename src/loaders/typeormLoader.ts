import {
    MicroframeworkLoader,
    MicroframeworkSettings,
} from 'microframework-w3tec';
import { createConnection, getConnectionOptions } from 'typeorm';
import { env } from '../env';

export const typeormLoader: MicroframeworkLoader = async (
    settings: MicroframeworkSettings | undefined,
) => {
    const loadedConnectionOptions = await getConnectionOptions();

    const connectionOptions = Object.assign(loadedConnectionOptions, {
        type: env.db.type as any, // See createConnection options for valid types
        host: env.db.host,
        port: env.db.port,
        username: env.db.username,
        password: env.db.password,
        database: env.db.database,
        synchronize: env.db.synchronize,
        logging: true,
        entities: env.app.dirs.entities,
    });
    console.log('before connection');
    const connection = await createConnection(connectionOptions);
    console.log('after connection');

    if (settings) {
        settings.setData('connection', connection);
        settings.onShutdown(() => connection.close());
    }
};

