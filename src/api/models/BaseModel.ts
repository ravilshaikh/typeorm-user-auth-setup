import { Exclude } from 'class-transformer';
import {
    Column,
    CreateDateColumn,
    Index,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
@Index(['id'])
export class BaseModel {
    @PrimaryGeneratedColumn('increment')
    public id: number;
    @Exclude()
    @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
    public createdAt: Date;
    @Exclude()
    @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
    public updatedAt: Date;
    @Exclude()
    @Column({ default: false, name: 'is_deleted' })
    public isDeleted: boolean;
}
