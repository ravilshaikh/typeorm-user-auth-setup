import * as express from 'express';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import * as httpContext from 'express-http-context';

@Middleware({ type: 'before', priority: -1 })
export class ContextMiddleware implements ExpressMiddlewareInterface {
    public use(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): any {
        httpContext.ns.bindEmitter(req);
        httpContext.ns.bindEmitter(res);
        return httpContext.middleware(req, res, next);
    }
}
