import * as dotenv from 'dotenv';
import * as path from 'path';
import * as pkg from '../package.json';
import { getOsEnv, getOsPath, getOsPaths, toBool } from './lib/env/utils';

/**
 * Load .env file or for tests the .env.test file.
 */
dotenv.config({
    path: path.join(
        process.cwd(),
        `.env${process.env.NODE_ENV === 'test' ? '.test' : ''}`,
    ),
});

/**
 * Environment variables
 */
export const env = {
    node: process.env.NODE_ENV || 'development',
    isProduction: process.env.NODE_ENV === 'production',
    isTest: process.env.NODE_ENV === 'test',
    isDevelopment: process.env.NODE_ENV === 'development',
    app: {
        name: process.env['APP_NAME'],
        version: (pkg as any).version,
        description: (pkg as any).description,
        host: process.env['APP_HOST'],
        schema: process.env['APP_SCHEMA'],
        routePrefix: process.env['APP_ROUTE_PREFIX'],
        port: process.env['APP_PORT'],
        banner: toBool(process.env['APP_BANNER']),
        dirs: {
            migrations: process.env['TYPEORM_MIGRATIONS'],
            migrationsDir: process.env['TYPEORM_MIGRATIONS_DIR'],
            entities: getOsPaths('TYPEORM_ENTITIES'),
            entitiesDir: getOsPath('TYPEORM_ENTITIES_DIR'),
            controllers: getOsPaths('CONTROLLERS'),
            middlewares: getOsPaths('MIDDLEWARES'),
            interceptors: getOsPaths('INTERCEPTORS'),
            subscribers: getOsPaths('SUBSCRIBERS'),
            resolvers: getOsPaths('RESOLVERS'),
        },
    },
    log: {
        level: process.env['LOG_LEVEL'],
        json: toBool(process.env['LOG_JSON']),
        output: process.env['LOG_OUTPUT'],
    },
    db: {
        type: process.env['TYPEORM_CONNECTION'],
        host: process.env['TYPEORM_HOST'],
        port: Number(process.env['TYPEORM_PORT']),
        username: process.env['TYPEORM_USERNAME'],
        password: process.env['TYPEORM_PASSWORD'],
        database: process.env['TYPEORM_DATABASE'],
        entities: getOsPaths('TYPEORM_ENTITIES'),
        entitiesDir: getOsPath('TYPEORM_ENTITIES_DIR'),
        synchronize: toBool(process.env['TYPEORM_SYNCHRONIZE']),
        logging: process.env['TYPEORM_LOGGING'],
    },
    swagger: {
        enabled: toBool(process.env['SWAGGER_ENABLED']),
        route: process.env['SWAGGER_ROUTE'],
        username: process.env['SWAGGER_USERNAME'],
        password: process.env['SWAGGER_PASSWORD'],
    },
    monitor: {
        enabled: toBool(process.env['MONITOR_ENABLED']),
        route: process.env['MONITOR_ROUTE'],
        username: process.env['MONITOR_USERNAME'],
        password: process.env['MONITOR_PASSWORD'],
    },
    redisCluster: {
        IS_REDIS_CLUSTER: toBool(process.env['IS_REDIS_CLUSTER']),
    },
    rateLimit: {
        API_REQUEST_LIMIT: Number(process.env['API_REQUEST_LIMIT']),
        API_RATE_TIME_IN_SEC: Number(process.env['API_RATE_TIME_IN_SEC']),
    },
    configRedis: {
        host: process.env['REDIS_HOST'],
        port: Number(process.env['REDIS_PORT']),
        password: process.env['REDIS_CLUSTER_PASSWORD'],
        enabled: toBool(process.env['REDIS_ENABLE']),
    },
    jwt: {
        SECRET: getOsEnv('JWT_SECRET'),
        EXPIRES_IN: getOsEnv('JWT_EXPIRES_IN'),
        RESET_PASSWORD_EXPIRES_IN: getOsEnv('JWT_RESET_PASSWORD_EXPIRES_IN'),
    },
    aws: {
        BUCKET: getOsEnv('AWS_BUCKET'),
        ACCESSKEY: getOsEnv('AWS_ACCESSKEY'),
        SECRETKEY: getOsEnv('AWS_SECRETKEY'),
        FOLDER: getOsEnv('AWS_FOLDER'),
        REGION: getOsEnv('AWS_REGION'),
    },
    sendGrid: {
        SENDGRID_API_KEY: String(process.env['SENDGRID_API_KEY']),
    },
    website: {
        WEBSITE_BASE_URL: process.env['BASE_URL'],
    },
    email: {
        no_reply_email: process.env['EMAIL_NO_REPLY'],
        help_email: process.env['EMAIL_HELP'],
        mail_trigger: toBool(process.env['MAIL_TRIGGER']),
    },
};
