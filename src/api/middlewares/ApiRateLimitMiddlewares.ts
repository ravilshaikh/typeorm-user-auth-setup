// import * as express from 'express';
// import rateLimit from 'express-rate-limit';
// import RedisStore from 'rate-limit-redis';
// import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
// import { env } from '../../env';
// import { RedisService } from '../../Redis/RedisService';
// @Middleware({ type: 'before' })
// export class ApiRateLimitMiddleware implements ExpressMiddlewareInterface {
//     public use(
//         req: express.Request,
//         res: express.Response,
//         next: express.NextFunction
//     ): any {
//         return rateLimit({
//             windowMs: env.rateLimit.API_RATE_TIME_IN_SEC * 1000,
//             max: env.rateLimit.API_REQUEST_LIMIT,
//             headers: true,
//             store: new RedisStore({
//                 client: RedisService.client,
//             }),
//         })(req, res, next);
//     }
// }
