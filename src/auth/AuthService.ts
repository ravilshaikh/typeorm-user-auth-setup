import * as express from 'express';
import * as httpContext from 'express-http-context';
import * as jwt from 'jsonwebtoken';
import { sign } from 'jsonwebtoken';
import { BadRequestError } from 'routing-controllers';
import { Service } from 'typedi';
import { User } from '../api/models/User';
import { MailService } from '../api/services/MailService';
import { UserService } from '../api/services/UserService';
import { TokenInterface} from "../api/controllers/requests/TokenInterface";
import { Logger, LoggerInterface } from '../decorators/Logger';
import { env } from '../env';
export class AuthResponse {
    public jwt: string;
}
@Service()
export class AuthService {
    constructor(
        @Logger(__filename) private log: LoggerInterface,
        @Service() private userService: UserService,
        private mail: MailService,
    ) {}

    public parseBasicAuthFromRequest(
        req: express.Request,
    ): {
        email: string;
        password: string;
        authType: string;
        user: TokenInterface;
    } {
        const authorization = req.header('Authorization');

        if (authorization && authorization.split(' ')[0] === 'Basic') {
            this.log.info('Credentials provided by the client');
            const decodedBase64 = Buffer.from(
                authorization.split(' ')[1],
                'base64',
            ).toString('ascii');
            const email = decodedBase64.split(':')[0];
            const password = decodedBase64.split(':')[1];
            if (email && password) {
                return { email, password, authType: 'Basic', user: undefined };
            }
        }

        this.log.info('No credentials provided by the client');
        return undefined;
    }

    public parseJWTAuthFromRequest(req: express.Request): Promise<any> {
        return new Promise((resolve, reject) => {
            const authorization = req.header('Authorization');
            if (authorization && authorization.split(' ')[0] === 'JWT') {
                this.log.info('Credentials provided by the client');
                jwt.verify(
                    authorization.split(' ')[1],
                    env.jwt.SECRET,
                    (err: Error, decoded: any) => {
                        if (err) {
                            return reject(err);
                        }
                        const data: TokenInterface = decoded;
                        const result = {
                            authType: 'JWT',
                            user: data,
                        };
                        return resolve(result);
                    },
                );
            } else {
                return resolve(undefined);
            }
        });
    }

    public async validateUser(
        email: string,
        password: string,
    ): Promise<TokenInterface> {
        const user = await this.userService.findByEmail(email);
        if (user && (await User.comparePassword(user, password))) {
            httpContext.set('requestUser', user);
            const validatedResponse = {
                userId: user.id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                mobileNumber:user.mobileNumber
            };
            return validatedResponse;
        }
        return undefined;
    }

    public async forgetMail(email: string): Promise<void> {
        const user = await this.userService.findByEmail(email);
        if (!user) {
            throw new BadRequestError(
                'This email is not registered with us, please signup.',
            );
        }
        const token = sign({ userId: user.id }, env.jwt.SECRET, {
            expiresIn: env.jwt.RESET_PASSWORD_EXPIRES_IN,
        });
        this.mail.forget(user, token);
        return;
    }

    public async login(body: any): Promise<AuthResponse | undefined> {
        if (!body.email || !body.password) {
            throw new BadRequestError(
                `Please fill required field Email and Password`,
            );
        }

        const user = await this.userService.findByEmail(body.email);
        if (!user) {
            throw new BadRequestError(
                'This email is not registered with us,please signup.',
            );
        }

        const status = await User.comparePassword(user, body.password);
        if (!status) {
            throw new BadRequestError('You have entered an incorrect password');
        }
       

        const token = sign(
            {
                userId: user.id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                mobileNumber:user.mobileNumber
            },
            env.jwt.SECRET,
            { expiresIn: env.jwt.EXPIRES_IN },
        );
        const authResponse = new AuthResponse();
        authResponse.jwt = token;
        return authResponse;
    }
}
