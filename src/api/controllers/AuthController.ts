import { MinLength } from 'class-validator';
import {
    Authorized,
    Body,
    JsonController,
    Param,
    Post,
    Put,
    Req,
} from 'routing-controllers';
import { Service } from 'typedi';
import { AuthService } from '../../auth/AuthService';
import { UserService } from './../services/UserService';
class AuthBody {
    public email: string;
    public password: string;
}

class AuthResponse {
    public jwt: string;
}

class MinPassword {
    @MinLength(5)
    public password: string;
}

@JsonController('/auth')
export class Auth {
    constructor(
        @Service() private userService: UserService,
        @Service() private authsSrvice: AuthService,
    ) {}

    @Post('/login')
    public async login(@Body() body: AuthBody): Promise<any | undefined> {
        const result = {};
        const data = await this.authsSrvice.login(body);
        Object.assign(result, data);
        result['messageKey'] =
            'You are loggedIn:';
        return result;
    }

    @Post('/forget/:email')
    public async forgetPassword(@Param('email') email: string): Promise<any> {
        return this.authsSrvice.forgetMail(email);
    }

    @Authorized()
    @Put('/reset')
    public async resetPassword(
        @Body() body: MinPassword,
        @Req() req: any,
    ): Promise<void> {
        const password = body.password;
        return this.userService.resetPassword(req, password);
    }

    @Authorized()
    @Put('/verify')
    public async verifyUserEmail(@Req() req: any): Promise<AuthResponse> {
        return this.userService.verifyUserEmail(req);
    }
}
