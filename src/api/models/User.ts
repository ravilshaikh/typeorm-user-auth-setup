import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IsEmail } from 'class-validator';
import {
    BeforeInsert,
    Column,
    Entity,
    Index,
} from 'typeorm';
import { BaseModel } from './BaseModel';

@Entity({ name: 'users' })
export class User extends BaseModel{
    public static hashPassword(password: string): Promise<string> {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                resolve(hash);
            });
        });
    }

    public static comparePassword(
        user: User,
        password: string,
    ): Promise<boolean> {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve(res === true);
            });
        });
    }

    @Column({ name: 'first_name', nullable: false })
    public firstName: string;

    @Column({ name: 'last_name', nullable: true })
    public lastName: string;

    @Index({ unique: true })
    @IsEmail()
    @Column({ name: 'email', nullable: false })
    public email: string;

    @Column({ nullable: false })
    @Exclude()
    public password: string;

    @Column({ name: 'mobile_number', nullable: true })
    public mobileNumber: string;

    @Exclude()
    @Column({ name: 'is_email_validated', nullable: true, default: false })
    public isEmailValidated: boolean;

    @BeforeInsert()
    public async hashPassword(): Promise<void> {
        this.password = await User.hashPassword(this.password);
    }
}
