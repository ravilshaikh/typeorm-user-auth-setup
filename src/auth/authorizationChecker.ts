import { Action, HttpError } from 'routing-controllers';
import { Container } from 'typedi';
import { Connection } from 'typeorm';
import { Logger } from '../lib/logger';
import { AuthService } from './AuthService';

export function authorizationChecker(
    connection: Connection,
): (action: Action, roles: any[]) => Promise<boolean> | boolean {
    const log = new Logger(__filename);
    const authService = Container.get<AuthService>(AuthService);

    return async function innerAuthorizationChecker(
        action: Action,
        roles: string[],
    ): Promise<boolean> {
        // TODO remove basic auth: FOR DEVELOPMENT ONLY
        let credentials = authService.parseBasicAuthFromRequest(action.request);
        console.log('ROLES for this ', roles);
        if (credentials === undefined) {
            log.warn('No Basic credentials given', credentials);
            try {
                credentials = await authService.parseJWTAuthFromRequest(
                    action.request,
                );
            } catch (err) {
                log.warn('No JWT credentials given', err);
                throw new HttpError(401, err);
            }
        }
        if (!credentials) {
            return false;
        }

        if (credentials.authType === 'Basic') {
            action.request.user = await authService.validateUser(
                credentials.email,
                credentials.password,
            );
        } else if (credentials.authType === 'JWT') {
            action.request.user = credentials.user;
        }
        if (action.request.user === undefined) {
            log.warn('Invalid credentials given');
            return false;
        }
        if (Array.isArray(roles) && roles.length) {
            if (roles.indexOf(action.request.user['roles']) !== -1) {
                return true;
            } else return false;
        }
        log.info('Successfully checked credentials');
        return true;
    };
}
