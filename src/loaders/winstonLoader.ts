import {
    MicroframeworkLoader,
    MicroframeworkSettings,
} from 'microframework-w3tec';
import { configure, format, transports } from 'winston';
// ! import WinstonCloudWatch from 'winston-cloudwatch';

import { env } from '../env';

export const winstonLoader: MicroframeworkLoader = (
    settings: MicroframeworkSettings | undefined,
) => {
    configure({
        transports: [
            new transports.Console({
                level: env.log.level,
                handleExceptions: true,
                format:
                    env.node !== 'development'
                        ? format.combine(format.json())
                        : format.combine(format.colorize(), format.simple()),
            }),
        ],
    });
    if (env.node === 'production') {
        // ! add(new WinstonCloudWatch({}));
        // config = {cloudWatchLogs?: CloudWatchLogs,
        //     level?: string;
        //     ensureLogGroup?: boolean;
        //     logGroupName?: string | (() => string);
        //     logStreamName?: string | (() => string);
        //     awsAccessKeyId?: string;
        //     awsSecretKey?: string;
        //     awsRegion?: string;
        //     awsOptions?: CloudWatch.Types.ClientConfiguration;
        //     jsonMessage?: boolean;
        //     messageFormatter?: (logObject: LogObject) => string;
        //     proxyServer?: string;
        //     uploadRate?: number;
        //     errorHandler?: ((err: Error) => void);
        //     silent?: boolean;
        //     retentionInDays?: number;}
    }
};
