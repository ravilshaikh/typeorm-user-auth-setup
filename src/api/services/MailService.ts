import { Service } from 'typedi';
import { env } from '../../env';
import { SendGridService } from './SendGridService';
import {User} from '../models/User';
const BASE_URL = env.website.WEBSITE_BASE_URL;
const NO_REPLY_EMAIL = env.email.no_reply_email;

@Service()
export class MailService {
    constructor(private sendGridService: SendGridService) {}

    public createAccount(firstName:string, lastName:string, email:string, token:string): void {
        let html = '';
        const subject =
            'Welcome to user-auth, please verify your email address';
        const validateLink = BASE_URL + '/validate?token=' + token;
        html = `<p>Hi ${firstName + ' ' + lastName+' '+validateLink},<br><br></p>`;

        return this.sendGridService.sendMail(
            NO_REPLY_EMAIL,
            email,
            subject,
            html,
        );
    }
    public forget(user: User, token: string) {
        // const link = BASE_URL + '/reset?token=' + token;
        // const helpLink = BASE_URL + '/services/help';
        const subject = 'Password Reset';
        const email = user.email;
        const html = `<p>Hi ${user.firstName + ' ' + user.lastName},<br><br></p>`;
        return this.sendGridService.sendMail(
            NO_REPLY_EMAIL,
            email,
            subject,
            html,
        );
    }
}