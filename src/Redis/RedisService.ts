import { Service } from 'typedi';
import { promisifyAll } from 'bluebird';
import { env } from '../env';
import { Logger, LoggerInterface } from '../decorators/Logger';
import IoRedis from 'ioredis';

let redis;
if (env.redisCluster.IS_REDIS_CLUSTER) {
    redis = new IoRedis.Cluster(
        [
            {
                port: env.configRedis.port,
                host: env.configRedis.host,
            },
        ],
        {
            dnsLookup: (address, callback) => callback(null, address, null),
            redisOptions: {
                tls: {},
                password: env.configRedis.password,
            },
        }
    );
} else {
    redis = new IoRedis(env.configRedis.port, env.configRedis.host);
}

promisifyAll(redis);
redis.on('connect', () => {
    console.log(
        `${'=======Connected with redis server Env:'}===========` +
            `Port:${env.configRedis.port}===========Host:${env.configRedis.host}`
    );
});

redis.on('error', err => {
    console.log(err);
    console.log('Error while creating connection');
});
@Service()
export class RedisService {
    constructor(@Logger(__filename) private log: LoggerInterface) {}
    static client: any = redis;
    public isEnabled() {
        return env.configRedis.enabled;
    }
    public getConnection() {
        return redis;
    }
    public async set(...params) {
        if (this.isEnabled()) {
            await redis.setAsync(params);
        } else {
            this.log.info('Redis is disabled');
        }
    }
    public async get(key) {
        if (this.isEnabled()) {
            // eslint-disable-next-line no-return-await
            return await redis.getAsync(key);
        }
        this.log.info('Redis is disabled');
        return null;
    }
    public async delete(key) {
        if (this.isEnabled()) {
            this.log.warn('Deleting Cache For', key);
            await redis.delAsync(key);
        } else {
            this.log.info('Redis is disabled');
        }
    }
    public async setObject(key, object, expiryTimeInSeconds = 86400, isCacheEnabled = 'true') {
        if (this.isEnabled() && isCacheEnabled === 'true') {
            if (object != null) {
                const objectStr = JSON.stringify(object);
                this.log.info('Setting cache for', key);
                await redis.setAsync(key, objectStr, 'EX', expiryTimeInSeconds);
            }
        } else {
            this.log.info('Redis is disabled');
        }
    }
    public async getObject(key, isCacheEnabled = 'true') {
        if (this.isEnabled() && isCacheEnabled === 'true') {
            const strObject = await redis.getAsync(key);
            try {
                this.log.info('Fetching from cache for', key);
                return JSON.parse(strObject);
            } catch (err) {
                this.log.error('Error parsing json response from redis for key', key, err);
            }
        } else {
            this.log.info('Redis is disabled');
        }
        return null;
    }
    public async findKeys(pattern: string) {
        if (this.isEnabled()) {
            pattern = pattern + '*';
            const keys = await redis.keysAsync(pattern);
            try {
                return keys;
            } catch (err) {
                this.log.error('Error Findig keys');
            }
        } else {
            this.log.info('Redis is disabled');
        }
        return null;
    }
    public async findAllValues(keys: any) {
        const keyPromiseArray = [];
        if (this.isEnabled()) {
            for (let iKey of keys) {
                console.log('working on key - ' + iKey);
                let val = redis.getAsync(iKey);
                keyPromiseArray.push(val);
            }
            const keyValues = await Promise.all(keyPromiseArray);
            const redisKeyValueArray = [];
            keyValues.forEach((value, index) => {
                redisKeyValueArray[index] = {};
                redisKeyValueArray[index]['key'] = keys[index];
                if (keyValues[index]) {
                    try {
                        redisKeyValueArray[index]['value'] = JSON.parse(keyValues[index]);
                    } catch {
                        redisKeyValueArray[index]['value'] = keyValues[index];
                    }
                } else {
                    redisKeyValueArray[index]['value'] = {};
                }
            });
            return redisKeyValueArray;
        } else {
            this.log.info('Redis is disabled');
        }
        return null;
    }
}
