import * as AWS from 'aws-sdk';
import { env } from '../../env';

export class AWSS3 {
    constructor() {
        console.log('INSIDE CONSTRUCTOR', env.aws);
        AWS.config.update({
            credentials: {
                accessKeyId: env.aws.ACCESSKEY,
                secretAccessKey: env.aws.SECRETKEY,
            },
            region: env.aws.REGION,
        });
        this.s3 = new AWS.S3();
    }
    public s3: AWS.S3;
    public async uploadToS3(
        fileName: string,
        body: any,
        contentType: string,
        bucket: string = env.aws.BUCKET,
        subpath = 'portfolio/',
        acl = 'public-read',
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            const params = {
                ACL: acl,
                Body: body,
                Key: `${subpath}${fileName}`,
                Bucket: bucket,
                ContentType: 'image/jpeg',
            };
            if (contentType) {
                params.ContentType = contentType;
            }
            console.log('config is', this.s3.config);
            this.s3.upload(params, (err, data) => {
                if (err) {
                    console.log('ERRROOR', err);
                    return reject(err);
                }
                return resolve(data);
            });
        });
    }
    public async listAllObjects(params): Promise<any> {
        try {
            const allKeys = await this.s3.listObjectsV2(params).promise();
            if (!allKeys.IsTruncated) {
                return allKeys.Contents;
            } else {
                params['ContinuationToken'] = allKeys['ContinuationToken'];
                return allKeys.Contents.concat(
                    await this.listAllObjects(params),
                );
            }
        } catch (err) {
            return err;
        }
    }

    public async getObjectFromS3(params) {
        return this.s3.getObject(params).promise();
    }
    public async deleteObjectFromS3(params) {
        return this.s3.deleteObject(params).promise();
    }
}
