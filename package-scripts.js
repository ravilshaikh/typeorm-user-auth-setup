const { series, rimraf } = require('nps-utils');

module.exports = {
    scripts: {
        default: 'nps start',
        /**
         * Starts the builded app from the dist directory.
         */
        start: {
            script: 'cross-env NODE_ENV=production node dist/src/app.js',
            description: 'Starts the builded app',
        },
        serve: {
            inspector: {
                script: series('nodemon --watch src --watch .env --inspect'),
                description:
                    'Serves the current app and watches for changes to restart it, you may attach inspector to it.',
            },
            script: series('nodemon --watch src --watch .env'),
            description:
                'Serves the current app and watches for changes to restart it',
        },
        config: {
            script: series(runFast('./commands/tsconfig.ts')),
            hiddenFromHelp: true,
        },
        build: {
            script: series(
                'nps config',
                // 'nps lint',
                'nps clean.dist',
                'nps transpile',
                'nps copy',
                'nps copy.tmp',
                'nps clean.tmp'
            ),
            description: 'Builds the app into the dist directory',
        },
        transpile: {
            script: `tsc --project ./tsconfig.build.json`,
            hiddenFromHelp: true,
        },
        clean: {
            default: {
                script: series(`nps clean.dist`),
                description: 'Deletes the ./dist folder',
            },
            dist: {
                script: rimraf('./dist'),
                hiddenFromHelp: true,
            },
            tmp: {
                script: rimraf('./.tmp'),
                hiddenFromHelp: true,
            },
        },
        copy: {
            default: {
                script: series(
                    `nps copy.public`,
                    `nps copy.script1`,
                    `nps copy.script2`,
                    `nps copy.script3`,
                    `nps copy.script4`,
                    `nps copy.script5`
                ),
                hiddenFromHelp: true,
            },
            public: {
                script: copy('./src/public/*', './dist'),
                hiddenFromHelp: true,
            },
            script1: {
                script: `copyfiles package.json dist/`,
                hiddenFromHelp: true,
            },
            script2: {
                script: `copyfiles package-scripts.js dist/`,
                hiddenFromHelp: true,
            },
            script3: {
                script: `copyfiles .npmrc dist/`,
                hiddenFromHelp: true,
            },
            script4: {
                script: `copyfiles env.common dist/`,
                hiddenFromHelp: true,
            },
            script5: {
                script: `copyfiles .env.* dist/`,
                hiddenFromHelp: true,
            },
            tmp: {
                script: copyDir('./.tmp/', './dist'),
                hiddenFromHelp: true,
            },
        },
    },
};
function runFast(path) {
    return `ts-node --transpile-only ${path}`;
}
function copy(source, target) {
    return `copyfiles --up 1 ${source} ${target}`;
}

function copyDir(source, target) {
    return `ncp ${source} ${target}`;
}
