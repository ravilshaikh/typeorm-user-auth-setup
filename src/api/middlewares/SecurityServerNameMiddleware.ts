import { Middleware, ExpressMiddlewareInterface } from 'routing-controllers';
import * as express from 'express';
@Middleware({ type: 'before' })
export class SecurityNoCacheMiddleware implements ExpressMiddlewareInterface {
    public use(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): any {
        res.removeHeader('Server');
        next();
    }
}
