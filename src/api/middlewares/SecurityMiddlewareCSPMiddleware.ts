import * as express from 'express';
import helmet from 'helmet';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';

@Middleware({ type: 'before' })
export class SecurityMiddlewareCSP implements ExpressMiddlewareInterface {
    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        return helmet({
            contentSecurityPolicy: {
                directives: {
                    'default-src': ["'self'"],
                    'base-uri': ["'self'"],
                    'block-all-mixed-content': true,
                    'font-src': ["'self'"],
                    'frame-ancestors': ["'self'"],
                    'img-src': ["'self'"],
                    'object-src': ['"none"'],
                    scriptSrc: ['"self"'],
                    styleSrc: ['"self"'],
                    'upgrade-insecure-requests': true,
                },
            },
        })(req, res, next);
    }
}
