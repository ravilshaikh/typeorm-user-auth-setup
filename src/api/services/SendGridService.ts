import sgMail from '@sendgrid/mail';
import { Service } from 'typedi';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { env } from '../../env';

@Service()
export class SendGridService {
    constructor(@Logger(__filename) private log: LoggerInterface) {
        sgMail.setApiKey(env.sendGrid.SENDGRID_API_KEY);
    }

    public sendMail(from: string, to: any, subject: string, html: string) {
        const msg = {
            to: to,
            from: from,
            subject: subject,
            html: html,
        };
        if (env.email.mail_trigger) {
            sgMail
                .send(msg)
                .then((result) => {
                    this.log.info('mail =>' + result);
                })
                .catch((err) => {
                    this.log.warn(err);
                });
        } else {
            this.log.warn(
                'Email triggering is disables, no email triggered for',
                to,
            );
        }
    }
}
